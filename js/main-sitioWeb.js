// Configuración FireBase
//
// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
import { getFirestore, doc, getDoc, getDocs, collection } from
"https://www.gstatic.com/firebasejs/9.4.0/firebase-firestore.js";
import { getStorage, ref as refS, uploadBytes, getDownloadURL } from
"https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js";
import { getDatabase,onValue,ref,set,child,get,update,remove } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js";

// Login
import { getAuth, signInWithEmailAndPassword, onAuthStateChanged, signOut } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-auth.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDcuccjXP-u3CKSSdHhrTEX-jfMoYX8EPY",
  authDomain: "ronaldoviera-proyecto.firebaseapp.com",
  projectId: "ronaldoviera-proyecto",
  storageBucket: "ronaldoviera-proyecto.appspot.com",
  messagingSenderId: "538126744130",
  appId: "1:538126744130:web:e231db99638c8f02a74bdb",
  measurementId: "G-V7CBGCPV89"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();

//Botones login
var botonLogin = document.getElementById('btnLogin');
var btnDesconectar = document.getElementById('btnDesconectar');

//Textos para login
var txtCorreo="";
var txtContra="";

// Initialize Firebase Authentication and get a reference to the service
const auth = getAuth(app);

// Función para leer los inputs del index-admin.html
function leerLogin(){
    txtCorreo=document.getElementById('inpCorreo').value;
    txtContra=document.getElementById('inpContra').value;
}

function ComprobarAuten(){
    onAuthStateChanged(auth, (user) => {
        if (user) {
          alert("Usuario encontrado");
        } else {
          alert("No se encontró un usuario");
          window.location.href="index-sitioWeb.html";
        }
    });
}

if(window.location.href == "https://dropsical-clump.000webhostapp.com/html/admin.html"){
        window.onload = ComprobarAuten();
}

if(botonLogin){
    botonLogin.addEventListener('click', (e)=>{
        e.preventDefault();
      leerLogin();
      signInWithEmailAndPassword(auth, txtCorreo, txtContra)
        .then((userCredential) => {
          // Inicio de sesión exitoso
          const user = userCredential.user;
          alert("Iniciaste sesión.")
          window.location.href="admin.html";
          //...
        })
        .catch((error) => {
          alert("Usuario o contraseña erroneos.")
          const errorCode = error.code;
          const errorMessage = error.message;
        });
    });
}

if(btnDesconectar){
    btnDesconectar.addEventListener('click',  (e)=>{
      signOut(auth).then(() => {
        alert("SESIÓN CERRADA")
        window.location.href="index-sitioWeb.html";
        // Sign-out successful.
      }).catch((error) => {
        // An error happened.
      });
    });
  }

// Objetos del Panel de Administrador
var codigo = "";
var precio = "";
var nombre = "";
var descripcion = "";
var estado = "";
var url = "";

// Botones para Panel de Administrador
var lista = document.getElementById('lista');
var btnAgregar = document.getElementById('btnAgregar');
var btnConsultar = document.getElementById('btnConsultar');
var btnActualizar = document.getElementById('btnActualizar');
var btnDesHabilitar = document.getElementById('btnDesHabilitar');
var btnMostrarRegistros = document.getElementById('btnMostrarRegistros');
var btnLimpiar = document.getElementById('btnLimpiar');

var archivo = document.getElementById('archivo');
var marcoProductos = document.getElementById('marcoProductos');

if(window.location.href == "http://127.0.0.1:5501/html/productos.html"){
  window.onload = mostrarProductos();
}

// Función para agregar articulos en la pagina de productos del Sitio Web
function mostrarProductos(){

    const db = getDatabase();
    const dbRef = ref(db, 'productos');
    onValue(dbRef, (snapshot) => {
        if(lista){
            lista.innerHTML = "";
        }
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
            console.log(lista);
            console.log(marcoProductos);
            if(lista){
                lista.innerHTML = lista.innerHTML + "<div class='tablas'> " + "<img class='imgAdmin' src=' " + childData.url + "'>"+"<h2>Codigo: " + childKey + "</h2><h2>Nombre: " + childData.nombre + "</h2><p>Descripción:"  + childData.descripcion + "</p><p>URL imagen:"  + childData.url + "</p><p> Precio: $" + childData.precio+ "</p>"+"<p> Estado:" + childData.estado + "</p></div>";
            }else if(marcoProductos){
                if(childData.estado==1){
                    marcoProductos.innerHTML= marcoProductos.innerHTML+"<div class='imagenes'><div class='producto'><picture><img class='imagen' src='"+childData.url+"'></picture><br><p class='jersey'>"+childData.nombre +"</p><p>"+childData.descripcion+"</p><p class='precio'>$"+childData.precio+"</p></div><br><a href='#'>COMPRAR</a></div>";
                }
            }
            
        });
    },{
        onlyOnce: true
    });
}

function leerInputs(){
    
    codigo = document.getElementById('codigo').value;
    nombre = document.getElementById('nombre').value;
    descripcion = document.getElementById('descripcion').value;
    precio = document.getElementById('precio').value;
    estado = document.getElementById('estado').value;
    url = document.getElementById('url').value;
}

function insertarDatos(){
    event.preventDefault();
    
    subirArchivo();
    setTimeout(leerInputs,5000);

    
        setTimeout(()=>{
            if(codigo=="" || nombre=="" || descripcion=="" || precio=="" || estado==""){
                alert("Asegurate de que llenaste todos los campos.");
            } else{
                set(ref(db,'productos/' + codigo),{
                    nombre: nombre,
                    descripcion:descripcion,
                    precio:precio,
                    estado:estado,
                    url:url})
                    .then((response) => {
                    alert("Registro con éxito");
                    mostrarProductos();
                    }).catch((error) => {
                    alert("Error al registrar.")
                });
            }
        }, 5000);
}

async function mostrarDatos(){
    event.preventDefault();
    leerInputs();
    const dbref = ref(db);
    
    if(codigo!=""){
        await get(child(dbref,'productos/' + codigo)).then((snapshot)=>{
            if (snapshot.exists()) {
                nombre = snapshot.val().nombre;
                descripcion = snapshot.val().descripcion;
                precio = snapshot.val().precio;
                estado = snapshot.val().estado;
                url = snapshot.val().url;
    
                escribirInputs();
                
            }else{
                alert("No existe el producto.");
            }
        }).catch((error)=>{
            alert("Se encontró un error: " + error);
        });
    } else{
        alert("No existe el producto.");
    }
        
    }

function actualizar(){
    event.preventDefault();

    subirArchivo();

    setTimeout(leerInputs,5000);
    const dbref = ref(db);
    
    setTimeout(()=>{
        if(codigo!="" && nombre!="" && descripcion!="" && precio!="" && estado!=""){
            get(child(dbref,'productos/' + codigo)).then((snapshot)=>{
                if (snapshot.exists()) {
                    update(ref(db,'productos/'+ codigo),{
                        nombre: nombre,
                        descripcion:descripcion,
                        precio:precio,
                        estado:estado,
                        url:url
                    }).then(()=>{
                        alert("Actualizado.");
                        mostrarProductos();
                    })
                    .catch((error)=>{
                        alert("Se encontró un error. " + error );
                    });
                    
                }else{
                    alert("No existe el producto.");
                }
            }).catch((error)=>{
                alert("Se encontró un error: " + error);
            });
    } else{
        alert("Asegurate de que llenaste todos los campos.");
    }
    }, 5000);
}

function escribirInputs(){
    event.preventDefault();

    document.getElementById('codigo').value= codigo;
    document.getElementById('nombre').value=nombre;
    document.getElementById('descripcion').value=descripcion;
    document.getElementById('precio').value=precio;
    document.getElementById('estado').value=estado;
    document.getElementById('url').value=url;
}

async function desHabilitar(){
    event.preventDefault();

    leerInputs();
    const dbref = ref(db);
    
    if(codigo!=""){
        await get(child(dbref,'productos/' + codigo)).then((snapshot)=>{
            if (snapshot.exists()) {
                update(ref(db,'productos/'+ codigo),{
                    estado:"0"
                   }).then(()=>{
                    alert("Se deshabilitó el registro.");
                    mostrarProductos();
                   })
                   .catch((error)=>{
                    alert("Se encontró un error. " + error );
                   });
                
            }else{
                alert("No existe el producto.");
            }
        }).catch((error)=>{
            alert("Se encontró un error: " + error);
        });
    } else{
        alert("No existe el producto.");
    }
}

function limpiar(){
    event.preventDefault();

    lista.innerHTML="";
    codigo="";
    nombre="";
    descripcion="";
    precio="";
    estado=0;
    url="";
    document.getElementById('MuestraImagen').src="";
    escribirInputs();
}

var file="";
var name="";

async function cargarImagen(){
    event.preventDefault();

    // archivo seleccionado
    file=event.target.files[0];
    name=event.target.files[0].name;

}

async function subirArchivo(){
    event.preventDefault();

    const storage = getStorage();
    const storageRef = refS(storage, 'imagenes/' + name);

    // 'file' comes from the Blob or File API
    await uploadBytes(storageRef, file).then((snapshot) => {
        alert("Se cargó el archivo");
    });

    descargarImagen();
}

async function descargarImagen(){
    event.preventDefault();
    //alert(nombre);
    const storage = getStorage();
    const storageRef = refS(storage, 'imagenes/'+name);

    // Get the download URL
    await getDownloadURL(storageRef)
    .then((url) => {
    // Insert url into an <img> tag to "download"
    //console.log(url);
    document.getElementById('url').value = url;
    //document.getElementById('MuestraImagen').src=url;
    })
    .catch((error) => {
    // A full list of error codes is available at
    // https://firebase.google.com/docs/storage/web/handle-errors
        switch (error.code) {
            case 'storage/object-not-found':
            // File doesn't exist
            alert("El archivo no existe. " + error );
            break;
            case 'storage/unauthorized':
            // User doesn't have permission to access the object
            alert("El usuario no tiene permiso para acceder a este archivo. " + error );
            break;
            case 'storage/canceled':
            // User canceled the upload
            alert("El usuario canceló la subida del archivo. " + error );
            break;

            // ...

            case 'storage/unknown':
            // Unknown error occurred, inspect the server response
            alert("Ocurrió un error desconocido. " + error );
            break;
        }
    });
}

// Evento change
if(archivo){
    archivo.addEventListener('change',cargarImagen);
}

// Eventos click
if(btnAgregar){
    btnAgregar.addEventListener('click',insertarDatos);
}
if(btnConsultar){
    btnConsultar.addEventListener('click',mostrarDatos);
}
if(btnActualizar){
    btnActualizar.addEventListener('click',actualizar);
}
if(btnDesHabilitar){
    btnDesHabilitar.addEventListener('click',desHabilitar);
}
if(btnMostrarRegistros){
    btnMostrarRegistros.addEventListener('click',mostrarProductos);
}
if(btnLimpiar){
    btnLimpiar.addEventListener('click',limpiar);
}